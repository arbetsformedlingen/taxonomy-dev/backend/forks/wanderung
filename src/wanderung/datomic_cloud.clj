(ns wanderung.datomic-cloud
  (:require [datomic.client.api :as d]
            [wanderung.datom :as datom]
            [wanderung.datom-processor :as dp]
            [clojure.spec.alpha :as spec]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [clojure.tools.logging :as log]))

;;;------- Datomic cloud specific -------

(defn rate-limiter
  ([] (rate-limiter 2.0))
  ([period-seconds]
   (let [period-ns (long (* 1.0e9 period-seconds))
         state (atom 0)]
     #(apply < (swap-vals!
                state
                (fn [next-notification-time]
                  (let [current-time (System/nanoTime)]
                    (if (<= next-notification-time current-time)
                      (+ current-time period-ns)
                      next-notification-time))))))))

(def system-attribs-filename "datomic_system_attributes.edn")

(defn load-system-attribs 
  "Loads the standard system attributes, such as value types and cardinalities of the system attributes."
  []
  (-> system-attribs-filename
      io/resource
      slurp
      edn/read-string))

(defrecord TempId [value])

(defn temp-id
  "Produces an instance of TempId that wraps a temp-id."
  [eid]
  (TempId.
   (if (string? eid)
     eid
     (str "tmp-" eid))))

(def transaction-temp-id-str "datomic.tx")
(def transaction-temp-id (TempId. transaction-temp-id-str))

(defn temp-id?
  "Tests if something is a TempId."
  [x]
  (instance? TempId x))

(defn unwrap-temp-ids
  "Removes the TempId wrapper around the temp-id strings."
  [v]
  (mapv #(if (temp-id? %) (:value %) %) v))

(def system-attributes #{:db.install/valueType :db/valueType :db/cardinality :db/unique})

(defn map-datom [state [e a v tx added]]
  (let [attrib-data (dp/entity-attributes state a [:db/ident :db/valueType])
        new-a (:db/ident attrib-data a)
        is-ref (= :db.type/ref (:db/valueType attrib-data))
        new-v (when (and is-ref (system-attributes new-a))
                (:db/ident (dp/entity-attributes state v [:db/ident])))
        result [e new-a (or new-v v) tx added]]
    result))

(defn system-datom? [state [e a v _tx _added]]
  (or (and (= :db/ident a)
           (keyword? v)
           (#{"db" "db.part"} (namespace v)))
      (#{:fressian/tag} a)
      (#{[(dp/ident-eid state :db/ident) :db/doc]} [e a])))

(defn some-system-datoms? [state datom-set]
  (->> datom-set
       (some #(system-datom? state %))
       boolean))

(defn transaction? [x]
  (and (map? x)
       (contains? x :data)
       (contains? x :t)))

(defn system-transaction? [state transaction]
  {:pre [(transaction? transaction)]}
  (some-system-datoms? state (:data transaction)))

(defn post-map-transaction [state transaction]
  (assoc transaction :system-transaction (system-transaction? state transaction)))

(defn keep-datom? [datom]
  (not= :db.install/attribute (datom/datom-attribute datom)))

(defn map-transaction [state transaction]
  (->> transaction
       :data
       (into [] (comp (map #(map-datom state %))
                      (filter keep-datom?)))
       (assoc transaction :data)
       (post-map-transaction state)))

(defn system-transactions-are-first? [state]
  {:pre [(map? state)]}
  (let [mt (:mapped-transactions state)
        _ (when (not (vector? mt))
            (throw (ex-info
                    "Value at :mapped-transactions must be a vector"
                    state)))
        n (count mt)]
    (or (< n 2)
        (nth mt (- n 2))
        (not (nth mt (- n 1))))))

(defn check-that-system-transactions-are-first [state]
  (when-not (system-transactions-are-first? state)
    (throw (ex-info "A system transaction cannot follow a non-system transaction"
                    {:state state})))
  state)

(defn accumulate-mapped-transactions [state transaction]
  (-> state
      (update :mapped-transactions conj
              (map-transaction state transaction))
      check-that-system-transactions-are-first))

(defn interpret-transaction [state transaction]
  (let [n (-> state :mapped-transactions count)
        states (dp/interpret-transaction state (:data transaction))
        rl (:rate-limiter state)]
    (when (rl)
      (log/debug "Interpret transaction" n))
    (-> states
        last
        (accumulate-mapped-transactions transaction))))

(defn interpret [transactions]
  (reduce interpret-transaction
          (assoc dp/initial-state
                 :mapped-transactions []
                 :rate-limiter (rate-limiter))
          transactions))

(defn flat-mapped-datoms [state]
  (into []
        (comp (remove :system-transaction)
              (mapcat :data))
        (:mapped-transactions state)))

(defn transactions [db]
  (->> db
       (d/q '[:find ?tx ?inst
              ;; Add row :keys tx inst
              :in $
             :where
             [?tx :db/txInstant ?inst]])
       (map (juxt :tx :inst))
       sort))

(defn elements-are-sorted? [[f & _ :as elements]]
  (loop [x f
         elements elements]
    (if (empty? elements)
      true
      (let [[y & elements] elements]
        (if (= 1 (compare x y))
          false
          (recur y elements))))))

(defn ordered-txs? [txs]
  (and (elements-are-sorted? (map :tx txs))
       (elements-are-sorted? (map :inst txs))))

(defn reconstruct-transactions-from-datoms [datoms]
  (let [counter (atom 0)
        rl (rate-limiter)
        visit-datom (fn [datom]
                      (let [i (swap! counter inc)]
                        (when (rl)
                          (log/debug "Fetch datom" i)))
                      datom)
        datom-groups (group-by (fn [[_e _a _v t _added]] t)
                               (map visit-datom datoms))]
    (assert (every? number? (keys datom-groups)))
    (->> datom-groups
         (sort-by key)
         (map (fn [[k v]] {:t k :data v})))))

(defn reconstruct-transactions
  "Similar to tx-range but not limited to 100000 rows in a transaction."
  [conn]
  (let [db (d/db conn)
        txs (transactions db)]
    (when-not (ordered-txs? txs)
      (throw (ex-info "Transaction ids are not chronological" {:txs txs})))
    (-> db
        d/history ;; <-- include both added and retracted
        (d/datoms {:index :eavt :limit -1})
        reconstruct-transactions-from-datoms)))

(defn extract-datomic-cloud-data [conn]
  (->> conn
       reconstruct-transactions
       interpret
       flat-mapped-datoms))

;; A data-transaction-target is used for testing
;; to simulate a Datomic database.
(def data-transaction-target {:temp-ids {}
                              :transactions []})
(defn data-transaction-target? [x]
  (and (map? x)
       (contains? x :temp-ids)))

(defn transaction-datoms-last-order
  "This function is used as a key when ordering the datoms of a transaction. It does that by providing a higher key for datoms related to the transaction. That way, if a large transaction needs to be split into smaller transactions, the transaction-related information will be applied to the last of those transactions."
  [datom]
  (if (datom/refers-to-this-transaction? datom) 1 0))

(defn sort-and-partition-by [f data]
  (->> data
       (sort-by f)
       (partition-by f)))

(defn initialize-push-state [extra-state]
  (merge dp/initial-state
         {:temp-ids {}
          :system-attribs (load-system-attribs)
          :datom-counter 0
          :rate-limiter (rate-limiter)

          :atomic-counter 0

          ;; Modern versions of datomic seems to have a
          ;; limitation of at most 100000 elements.
          ;; You may want to set this value to 100000 in that case.
          :max-transaction-size nil
          
          ;; This function is used to modify the data associated with a transaction
          ;; before performing the transaction. By default, it does not modify anything.
          :transaction-info-for-batch (fn [transaction-info _batch-context] transaction-info)

          :partition-index-fn transaction-datoms-last-order
          :stats []}
         extra-state))

(defn push-state? [x]
  (and (dp/state? x)
       (contains? x :target)))

(defn validate-tx-row [[add-or-retract eid _a _v :as tx-row]]
  (assert (#{:db/add :db/retract} add-or-retract))
  (assert (some? eid))
  tx-row)

(defn data-target-transact [target tx-data]
  (let [current-temp-ids (:temp-ids target)
        temp-id-offset (count current-temp-ids)
        all-mentioned-temp-ids (into #{} (comp cat (filter temp-id?) (map :value)) tx-data)
        _ (assert (empty? (filter current-temp-ids all-mentioned-temp-ids)))
        new-tmp (into {}
                      (comp (remove current-temp-ids)
                            (map-indexed (fn [i k] [k (+ i temp-id-offset)])))
                      all-mentioned-temp-ids)
        temp-ids (merge current-temp-ids new-tmp)
        map-element #(if (temp-id? %)
                       (let [id (-> % :value temp-ids)]
                         (assert (number? id))
                         id)
                       %)
        map-row #(validate-tx-row (mapv map-element %))
        mapped (mapv map-row tx-data)]
    [(-> target
         (assoc :temp-ids (dissoc temp-ids transaction-temp-id-str))
         (update :transactions conj mapped))
     new-tmp]))

(defn target-transact [target tx-data]
  (if (data-transaction-target? target)
    (data-target-transact target tx-data)
    (let [conn target
          mapped-tx-data (map unwrap-temp-ids tx-data)]
      [conn (:tempids (d/transact conn {:tx-data mapped-tx-data}))])))

(defn sys-entity-attributes
  "Like entity-attributes, but also looking at built-in datomic attributes."
  ([state a] (sys-entity-attributes state a nil))
  ([state a select]
   {:pre [(push-state? state)]}
   (or (get-in state [:system-attribs a])
       (dp/entity-attributes state
                             (get-in state [:idents a] a)
                             select))))

(defn- register-new-eid
  "Used to keep track of what eids are part of this transaction."
  [state [e _ _ _ _]]
  (update state :new-eids conj e))

(defn lookup-temp-id
  ([temp-ids k] (lookup-temp-id temp-ids k nil))
  ([temp-ids k default-value]
   (cond
     (temp-id? k) (get temp-ids (:value k) default-value)
     (string? k) (get temp-ids k default-value)
     :else (throw (ex-info "Type not supported" {:key k})))))

(defn- map-value-ref
  "Maps the value part of a datom to either a temp id or a true entity id"
  [{:keys [temp-ids new-eids]} v]
  (if (keyword? v)
    v
    (or (lookup-temp-id temp-ids (temp-id v))
        (when (new-eids v)
          (temp-id v)))))

(defn filter-mentioning [f rows]
  (filter #(some f %) rows))

(defn remap-value [state [_e a v t _added]]
  (let [attrib-data (sys-entity-attributes state a)]
    (when-let [value-type (:db/valueType attrib-data)]
      (cond
        (not= :db.type/ref value-type) [v]
        (= v t) [transaction-temp-id]
        :else (let [newv (map-value-ref state v)]
                (when newv [newv]))))))

(defn remap-eid [{:keys [temp-ids]} [e _a _v t _added]]
  ;; The entity id: either it is a temp id for a new entity,
  ;; or a true entity id. In the special case that the entity
  ;; id is *this* transaction,
  ;; the special tempid "datomic.tx" is used.
  (if (= e t)
    transaction-temp-id
    (let [temp-eid (temp-id e)]
      (lookup-temp-id temp-ids
                      (:value temp-eid)
                      temp-eid))))


(defn map-transaction-row
  "Adds a tx-data row to the currently processed `:transaction-rows`, or postpones it to `:remaining`."
  [state [_e a _v _t added :as datom]]
  {:pre [(push-state? state)]}
  (when-let [[new-v] (remap-value state datom)]
    (update state
            :transaction-rows
            conj
            {:atomic-counter (:atomic-counter state)
             :tx-row (validate-tx-row
                      [
                       ;; Whether to add or retract
                       (if added :db/add :db/retract)

                       ;; Entity id
                       (remap-eid state datom)

                       ;; Attribute
                       a

                       ;; Value
                       new-v])})))

(defn map-datom-or-stop [state datom]
  (-> state
      (dp/register-datom datom)
      (register-new-eid datom)
      (dp/detect-ident-def datom)
      (map-transaction-row datom)
      (or (reduced nil))))

(defn push-atomic-datoms [state atomic-datom-group]
  {:pre [(push-state? state)]}
  (or (reduce map-datom-or-stop
              (update state :atomic-counter inc)
              atomic-datom-group)
      (update state :remaining conj atomic-datom-group)))

(defn push-step-remaining [state]
  {:pre [(push-state? state)]}
  (reduce push-atomic-datoms
          (assoc state :remaining [])
          (:remaining state)))

(defn valid-temp-ids? [x]
  (and (map? x)
       (every? string? (keys x))
       (every? number? (vals x))))

(defn check-remaining-is-empty [{:keys [remaining] :as state}]
  (when (seq remaining)
    (throw (ex-info "Failed to include datoms in transaction" {:remaining remaining})))
  state)

(defn transaction-info-kv [[op e a v]]
  (when (= e transaction-temp-id)
    [a {:value v :op op}]))

(defn tx-data-from-transaction-info [transaction-info]
  (into []
        (map (fn [[attrib {:keys [value op]}]]
               [(or op :db/add) transaction-temp-id-str attrib value]))
        transaction-info))

(defn weighted-partitioning
  ([weight-fn max-size] (weighted-partitioning weight-fn max-size conj))
  ([weight-fn max-size inner-step]
   (fn [step]
     (let [empty-part (inner-step)
           state-atom (atom [0 0 empty-part])]
       (fn
         ([] (step))
         ([dst] (step (let [[counter _ part] (deref state-atom)]
                        (if (zero? counter)
                          dst
                          (step dst part)))))
         ([dst x] (let [w (weight-fn x)
                        _ (when (< max-size w)
                            (throw (ex-info "Weight exceeds max-size" {:weight w :max-size max-size})))
                        [[counter-before _ part-before] [counter-after _ _]]
                        (swap-vals!
                         state-atom
                         (fn [[counter sum part]]
                           (let [next-sum (+ sum w)]
                             (if (< max-size next-sum)
                               [1 w (inner-step empty-part x)]
                               [(inc counter) next-sum (inner-step part x)]))))]
                    (if (< counter-before counter-after)
                      dst
                      (step dst part-before)))))))))

(defn with-default-batch [batches]
  (if (empty? batches)
    [[]]
    batches))

(defn split-into-batches [max-transaction-size marg]
  {:pre [(or (nil? max-transaction-size)
             (< marg max-transaction-size))]}
  (weighted-partitioning count
                         (if max-transaction-size
                           (- max-transaction-size marg)
                           Long/MAX_VALUE)
                         (cat conj)))

(defn transaction-row? [x]
  (and (map? x)
       (vector? (:tx-row x))
       (number? (:atomic-counter x))))

(defn split-large-transaction [{:keys [transaction-rows
                                       max-transaction-size tid
                                       transaction-info-for-batch
                                       observe-transaction] :as state}]
  {:pre [(every? transaction-row? transaction-rows)
         (or (nil? max-transaction-size)
             (number? max-transaction-size))]
   :post [(seq (:transaction-batches %))]}
  (when observe-transaction
    (log/info "split-large-transaction" transaction-rows))
  (let [transaction-info-from-row (comp transaction-info-kv :tx-row)
        transaction-info (-> (into {}
                                   (keep transaction-info-from-row)
                                   transaction-rows))
        transaction-info-size (count transaction-info)
        batch-margin (inc transaction-info-size)
        batches (with-default-batch ;; <-- there must be at least one batch to finalize.
                  (into []
                        (comp (remove transaction-info-from-row)
                              (partition-by :atomic-counter)
                              (split-into-batches max-transaction-size batch-margin))
                        transaction-rows))
        finalize-batch (fn [batch-index batch]
                         (let [context {:batch-count (count batches)
                                        :batch-index batch-index
                                        :tid tid}]
                           (-> transaction-info
                               (transaction-info-for-batch context)
                               tx-data-from-transaction-info
                               (into (map :tx-row) batch))))]
    (->> batches
         (map-indexed finalize-batch)
         (assoc state :transaction-batches))))

(defn map-temp-id [temp-ids row]
  (mapv (fn [x]
          (or (when (temp-id? x)
                (lookup-temp-id temp-ids x))
              x))
        row))

(defn commit-transaction [{:keys [temp-ids tid] :as state} transaction-rows]
  (log/trace "Transact" (count transaction-rows) "rows")
  (let [mapped-transaction-rows (map (partial map-temp-id temp-ids) transaction-rows)
        [target added-temp-ids] (target-transact (:target state)
                                                 mapped-transaction-rows)
        tx-kvpair (if-let [txid (get added-temp-ids transaction-temp-id-str)]
                    [[(-> tid temp-id :value) txid]]
                    [])
        temp-ids (-> temp-ids
                     (merge added-temp-ids)
                     (into tx-kvpair))]
    (assert (valid-temp-ids? added-temp-ids))
    (assert (number? tid))
    (assoc state
           :target target
           :temp-ids temp-ids)))

(defn commit-transactions [{:keys [transaction-batches observe-transaction] :as state}]
  {:pre [(push-state? state)]}
  (log/trace "Commit" (count transaction-batches) "transactions")
  (when observe-transaction
    (log/info "commit-transactions" transaction-batches))
  (reduce commit-transaction state transaction-batches))

(defn push-datom-partition [state datom-partition]
  (let [state (assoc state :current-datom-partition datom-partition)
        states (->> datom-partition
                    (update state :remaining into)
                    (dp/iterate-to-convergence push-step-remaining))]
    (log/trace "push-datom-partion converged after" (-> states count dec) "iterations")
    (last states)))

(defn push-datom-partitions [state datom-partitions]
  (reduce push-datom-partition
          (assoc state
                 :new-eids #{}
                 :transaction-rows []
                 :remaining [])
          datom-partitions))

(defn ea-key [[e a _ _ _]]
  [e a])

(defn add-order [datom]
  (if (datom/datom-add? datom) 1 0))

(defn atomic-groups-from-ea-group
  [state [[_e attrib] [{:keys [index]} :as group]]]
  (let [cardinality (dp/get-cardinality state attrib)

        ;; We assume that if cardinality is undefined, it is "one".
        is-cardinality-one (not= :db.cardinality/many cardinality)

        ;; On the other hand, if cardinality is "many", adding/removing
        ;; elements to the set of values probably does not need to happen
        ;; atomically. Not sure though.
        
        datoms (map :datom group)
        is-atomic-group (and is-cardinality-one
                             (< 1 (count datoms)))]
    
    (if is-atomic-group
      [{:index index
        :order 0
        :datoms (sort-by add-order (set datoms))}]
      (map (fn [{:keys [index datom]}]
             {:index index
              :order (add-order datom)
              :datoms [datom]})
           group))))

(defn sort-and-wrap-atomic-ops [state datoms]
  (->> datoms
       (map-indexed (fn [i datom]
                      {:index i
                       :datom datom}))
       (group-by (comp ea-key :datom))
       (mapcat #(atomic-groups-from-ea-group state %))
       (sort-by (juxt :order :index))
       (map :datoms)))

(defn push-transaction [{:keys [partition-index-fn rate-limiter datom-counter] :as state} transaction]
  {:pre [(push-state? state)]}
  (when (rate-limiter)
    (log/info "push-transaction at" datom-counter "completed datoms"))
  (let [state (-> state
                  (update :datom-counter + (count transaction))
                  (assoc :tid (-> transaction first datom/datom-tid)
                         :observe-transaction false
                         :current-transaction transaction))]
    (->> transaction
         (filter keep-datom?)
         (sort-and-partition-by partition-index-fn)
         (map #(sort-and-wrap-atomic-ops state %))
         (push-datom-partitions state)
         check-remaining-is-empty
         split-large-transaction
         commit-transactions)))

(defn push [state datoms]
  {:pre [(push-state? state)]}
  (transduce (partition-by datom/datom-tid)
             (completing push-transaction)
             state
             datoms))

(defn transact-datoms-to-datomic "Transact datoms to datomic. This function does the opposite of what the function `extract-datomic-cloud-data` does."
  ([conn datoms opts]
   {:pre [(spec/valid? :datom/tuples datoms)]}
   (log/info "Transact datoms to datomic with opts" opts)
   (push (initialize-push-state
          (merge {:target conn} opts))
         datoms)
   conn)
  ([conn datoms]
   (transact-datoms-to-datomic conn datoms {})))

(defn generate-system-attribs-file
  "Generate the `datomic_system_attribs.edn` file. The state passed in should only be from the first 6 interpreted transactions."
  [state]
  {:pre [(dp/state? state)]}
  (let [dst (io/file "resources" system-attribs-filename)]
    (io/make-parents dst)
    (spit dst (with-out-str
                (pp/pprint (dp/all-ident-attribs state))))))



(comment

  (do 
    (def sample-data (-> "sample_transactions.edn"
                         clojure.java.io/resource
                         slurp
                         edn/read-string))

    (def expected-datoms (-> "expected_extracted_cloud_data.edn"
                             clojure.java.io/resource
                             slurp
                             edn/read-string))
    
    (defn demo-interpret []
      (def result (interpret sample-data))
      (def datoms (flat-mapped-datoms result)))


    (defn demo-simulate []
      (def sim (push (initialize-push-state {:target data-transaction-target})
                     expected-datoms))
      )
  


    (generate-system-attribs-file (interpret (take 6 sample-data)))

    ))

(comment
  (do

    (require '[taoensso.nippy :as nippy])

    (def datoms (nippy/thaw-from-file "../taxonomy-database-dist/resources/taxonomy-v21.nippy"))

    (defn demo-simulate []
      (def sim (push (initialize-push-state
                      {:target data-transaction-target})
                     datoms)))    
    ))
