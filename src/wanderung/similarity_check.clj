(ns wanderung.similarity-check
  (:require [wanderung.datom :as datom]
            [wanderung.datom-processor :as dp]
            [clojure.tools.logging :as log]
            [clojure.set :as set])
  (:import [org.chocosolver.solver Model Solver]
           [org.chocosolver.solver.variables IntVar]))

(defn edge? [x]
  (and (vector? x)
       (= 3 (count x))))

(defn directed-graph? [x]
  (and (map? x)
       (map? (:vertices x))
       (let [edges (:directed-edges x)]
         (and (coll? edges)
              (every? edge? edges)))))

(defn valid-directed-graph? [x]
  (and (directed-graph? x)
       (let [referred-vertices (into [] (mapcat (fn [[a _ b]] [a b])) (:directed-edges x))
             vertex-set (-> x :vertices keys set)]
         (every? vertex-set referred-vertices))))

(defn integer-vertex-map [graph]
  {:pre [(directed-graph? graph)]}
  (-> graph :vertices keys (zipmap (range))))

(defn invert-map [m]
  (->> m
       (group-by val)
       (into {} (map (fn [[k group]] [k (map first group)])))))

(defn apply-vertex-map [graph vm]
  {:pre [(directed-graph? graph)
         (map? vm)]}
  (-> graph
      (update :vertices #(into {}
                               (map (fn [[k v]] [(vm k) v]))
                               %))
      (update :directed-edges #(into []
                                     (map (fn [[a label b]] [(vm a) label (vm b)]))
                                     %))))

(defn normalize-int-vertices [graph]
  (let [m (integer-vertex-map graph)]
    [(into {} (map (juxt val key)) m)
     (apply-vertex-map graph m)]))

(defn vertex-keys [graph]
  (-> graph :vertices keys))

(defn list-solutions [^Solver solver get-solution]
  (lazy-seq
   (when (.solve solver)
     (log/info "Solution found")
     (cons (get-solution)
           (list-solutions solver get-solution)))))

(defn invalidate [model]
  (-> model .falseConstraint .post))

(defn declare-variables [model digraph-a digraph-b]
  (let [data-vertices-b (-> digraph-b :vertices invert-map)]
    (into {}
          (map (fn [[vertex-a data-a]]
                 (let [cands (data-vertices-b data-a)
                       n (count cands)
                       inds (range n)
                       var-type (case n
                                  0 :infeasible
                                  1 :constant
                                  :variable)

                       ;; internal-var is a variable from 0..n-1
                       ;; and maps to an external-var in the domain cands.
                       ;; When we consider an edge between to vertices and
                       ;; their corresponding edge according to the mapping,
                       ;; we compute the index of the corresponding edge
                       ;; using the internal-vars of the vertices to
                       ;; keep the domain compact.

                       ;; Only introducing variables for domains greater than 1
                       ;; helps keeping the model small.
                       vars (case var-type
                              :infeasible (do (invalidate model) {})
                              :constant {:internal-var 0
                                         :external-var (first cands)}
                              :variable
                              (let [cand-array (int-array cands)
                                    internal-var (.intVar model
                                                          (str "internal-" vertex-a)
                                                          0
                                                          (dec n))
                                    external-var (.intVar model
                                                          (str "external-" vertex-a)
                                                          cand-array)]
                                (-> model
                                    (.element external-var cand-array internal-var)
                                    .post)
                                {:internal-var internal-var
                                 :external-var external-var}))]
                   [vertex-a (merge {:type var-type
                                     :inds inds
                                     :count n
                                     :decompress (zipmap inds cands)}
                                    vars)])))
          (:vertices digraph-a))))


(defmacro op-any [method numfn a b]
  `(case [(number? ~a) (number? ~b)]
     [false false] (~method ^IntVar ~a ^IntVar ~b)
     [false true] (~method ^IntVar ~a (int ~b))
     [true false] (~method ^IntVar ~b (int ~a))
     [true true] (~numfn ~a ~b)))

(defn mul-any [a b]
  (op-any .mul * a b))

(defn add-any [a b]
  (op-any .add + a b))

(defn add-edge-constraints [model digraph-a digraph-b a-cands-b]
  (let [b-edge-set (-> digraph-b :directed-edges set)]
    (into []
          (keep (fn [[src-a label dst-a]]
                  (let [src (a-cands-b src-a)
                        dst (a-cands-b dst-a)
                        dst-var-size (:count dst)

                        src-decompress (:decompress src)
                        dst-decompress (:decompress dst)

                        ;; This is the set of all possible edges that exist.
                        valid-inds (set (for [src-b (:inds src)
                                              dst-b (:inds dst)
                                              :when (b-edge-set [(src-decompress src-b)
                                                                 label
                                                                 (dst-decompress dst-b)])]

                                          ;; We map every edge tuple to an integer.
                                          (-> src-b
                                              (* dst-var-size)
                                              (+ dst-b))))
                        
                        constraint-is-redundant (and (= :constant src)
                                                     (= :constant dst)
                                                     (seq valid-inds))

                        ;; Map the edge endpoint variables to an integer that
                        ;; can be either an IntVar or an int (in case it is constant).
                        expr (-> src
                                 :internal-var
                                 (mul-any dst-var-size)
                                 (add-any (:internal-var dst)))]
                    (when-not constraint-is-redundant
                      (if (number? expr)
                        (when-not (contains? valid-inds expr)
                          (invalidate model))
                        (->> valid-inds
                             int-array
                             (.member model (.intVar expr))
                             .post)))
                    (when-not (seq valid-inds)
                      {:edge [src-a label dst-a]}))))
          (:directed-edges digraph-a))))

(defn get-value [x]
  (if (number? x) x (.getValue ^IntVar x)))

(defn solution-builder [amap bmap a-cands-b]
  #(into {}
         (map (fn [[k {:keys [external-var]}]]
                [(amap k) (-> external-var get-value bmap)]))
         a-cands-b))

(defn match-graphs [digraph-a digraph-b]
  {:pre [(valid-directed-graph? digraph-a)
         (valid-directed-graph? digraph-b)]}
  (let [ ;; Every vertex in digraph-a can map to a one of the vertices in digraph-b
        ;; that has the same data associated with it.
        [amap digraph-a] (normalize-int-vertices digraph-a)
        [bmap digraph-b] (normalize-int-vertices digraph-b)
        
        model (Model. "match-graphs")
        _ (log/info "Declare variables")
        a-cands-b (declare-variables model digraph-a digraph-b)
        all-variables-exist (every? (comp :internal-var val) a-cands-b)
        all-external-vars (->> a-cands-b
                               vals
                               (filter #(= :variable (:type %)))
                               (map :external-var))]

    (when all-variables-exist
      ;; Two different vertices in digraph-a cannot map to the same vertex
      ;; in digraph-b
      (log/info "Add all different constraints")
      (when (seq all-external-vars)
        (->> all-external-vars
             (into-array IntVar)
             (.allDifferent model)
             .post))
      
      (log/info "Add edge constraints")
      (add-edge-constraints model digraph-a digraph-b a-cands-b)
      ;; Lazy sequence of all solutions
      (log/info "Number of variables:" (.getNbVars model))
      (log/info "Number of constraints:" (.getNbCstrs model))
      (log/info "Run the solver")
      (-> model
          .getSolver
          (list-solutions (solution-builder amap bmap a-cands-b))))))

(defn accumulate-datoms [datoms]
  (transduce (comp (filter #(not= :db.install/attribute (datom/datom-attribute %)))
                   (partition-by datom/datom-tid))
             (completing (comp last dp/interpret-transaction))
             dp/initial-state
             datoms))

(defn get-attrib-eid [idents ak]
  (if (number? ak)
    ak
    (idents ak)))

(defn map-eids [{:keys [idents entities] :as state} f]
  {:pre [(dp/state? state)]}
  (let [rids (dp/ref-eids state)]
    {:entities (into {}
                     (for [[k {:keys [attributes]}] entities]
                       [(f k) {:attributes
                               (into {} (for [[ak av] attributes]
                                          (do (assert (keyword? ak))
                                              [(f ak)
                                               (if (rids (get-attrib-eid idents ak))
                                                 (if (number? av)
                                                   (f av)
                                                   (into #{} (map f) av))
                                                 av)]
                                              )))}]))
     :idents(into {} (for [[k v] idents]
                       [k (f v)]))}))

(defn labeled-edges [{:keys [idents entities] :as state}]
  (let [rids (dp/ref-eids state)]
    (for [[k {:keys [attributes]}] entities
          [ak av] attributes
          :when (rids (get-attrib-eid idents ak))
          dst (if (coll? av) av [av])]
      [k ak dst])))

(defn entity-feature-map [state]
  {:pre [(dp/state? state)]}
  (let [rids (dp/ref-eids state)
        idmap (:idents state)
        check-non-empty-attributes (fn [k x]
                                     (when (empty? x)
                                       (throw (ex-info "No attributes for entity" k)))
                                     x)]
    (into {}
          (map (fn [[k {:keys [attributes]}]]
                 [k (->> (for [[k v] attributes]
                           (do (assert (keyword? k))
                               (let [aid (idmap k nil)]
                                 [k
                                  (if (rids aid)
                                    (if (coll? v)
                                      (count v)
                                      nil)
                                    v)])))
                         (group-by first)
                         (map (fn [[k group]] [k (into #{} (map second) group)]))
                         (into {})
                         (check-non-empty-attributes k))]))
          (:entities state))))

(defn state-graph [state]
  {:post [(directed-graph? %)]}
  {:vertices (entity-feature-map state)
   :directed-edges (labeled-edges state)})

(defn datom-match-data-from-state [state]
  (let [graph (state-graph state)]
    {:state state
     :graph graph}))

(defn datom-match-data [datoms]
  {:pre [(sequential? datoms)]}
  (datom-match-data-from-state (accumulate-datoms datoms)))

(defn solve-graph-matching-problem [match-data-a match-data-b]
  (let [graph-a (:graph match-data-a)
        graph-b (:graph match-data-b)
        _ (log/info "Solve graph matching problem from"
                    (-> graph-a :vertices count)
                    "to"
                    (-> graph-b :vertices count) "vertices")
        solutions (match-graphs graph-a graph-b)
        issues (if (seq solutions)
                 []
                 ["No graph matching found"])]
    (log/info "Found" (count solutions) "solutions")
    {:a match-data-a
     :b match-data-b
     :issues issues
     :vertex-maps solutions}))

(defn solution-exists? [result]
  (-> result
      :vertex-maps
      seq
      boolean))

(defn similar-datoms? [datoms-a datoms-b]
  (solution-exists?
   (solve-graph-matching-problem
    (datom-match-data datoms-a)
    (datom-match-data datoms-b))))

(comment

  (do

    (require '[taoensso.nippy :as nippy])

    (defn transaction-related? [[e a v t _added]]
      (or (#{e v} t)
          (= a :taxonomy-version/tx)))
    
    (def datoms (-> "/home/jonas/prog/jobtech-taxonomy-api-backup-tool/debug_datoms.nippy"
                    nippy/thaw-from-file))
    (def datoms2 (-> "/home/jonas/prog/jobtech-taxonomy-api-backup-tool/debug_datoms2.nippy"
                     nippy/thaw-from-file))

    (def md (datom-match-data (remove transaction-related? datoms)))
    (def md2 (datom-match-data (remove transaction-related? datoms2)))

    (defn demo-solve []
      (def sol (solve-graph-matching-problem md md2)))
    )



  (do
    (def state (:state md))
    (def graph (:graph md))
    (def vertices (:vertices graph))
    (def freqs (-> vertices vals frequencies))
    (println (frequencies (vals freqs)))
    (into #{} (comp (filter #(< 1 (val %))) (map key)) freqs)
    
    )

  
  
  (do 
    (defn demo []
      (let [a {:vertices {1 "abc"
                          2 "xyz"
                          3 "abc"}
               :directed-edges [[1 :mjao 2]]}
            b {:vertices {20 "xyz"
                          119 "abc"
                          120 "abc"}
               :directed-edges [[120 :mjao 20]]}]
        (match-graphs a b)))

    (def sample-datoms (-> "expected_extracted_cloud_data.edn"
                           clojure.java.io/resource
                           slurp
                           clojure.edn/read-string))

    )
  (do
    (defn demo-fm []
      (def state (accumulate-datoms sample-datoms))
      (def fm (entity-feature-map state)))
    )

  


  )


