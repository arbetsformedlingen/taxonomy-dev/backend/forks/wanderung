(ns wanderung.db.api
  (:refer-clojure :exclude [sync])
  (:require [datomic.client.api :as dc]
            [datahike.api :as dh]))

(defn not-supported []
  (throw (ex-info "Not supported yet." {:error/cause :not-supported})))

(defprotocol Historian
  (db-stats [db])
  (history [db])
  (as-of [db time-point])
  (since [db time-point])
  (with [db arg-map]))

(defprotocol Searcher
  (datoms [db arg-map])
  (pull [db arg-map] [db selector eid])
  (index-range [db arg-map]))

(defrecord DatahikeDb [state]
  Historian
  (db-stats [_db] (not-supported))
  (history [_db] (map->DatahikeDb {:state (dh/history state)}))
  (as-of [_db time-point] (map->DatahikeDb {:state (dh/as-of state time-point)}))
  (since [_db time-point] (map->DatahikeDb {:state (dh/since state time-point)}))
  (with [_db {:keys [tx-data]}]
    (dh/with state tx-data))

  Searcher
  (datoms [_db {:keys [index components]}]
    (dh/datoms state index components))
  (pull [_db {:keys [selector eid]}]
    (dh/pull state selector eid))
  (pull [_db selector eid]
    (dh/pull state selector eid))
  (index-range [_db _arg-map] (not-supported)))

(defrecord DatomicDb [state]
  Historian
  (db-stats [_db] (not-supported))
  (history [_db] (map->DatomicDb {:state (dc/history state)}))
  (as-of [_db time-point] (map->DatomicDb {:state (dc/as-of state time-point)}))
  (since [_db time-point] (map->DatomicDb {:state (dc/since state time-point)}))
  (with [_db arg-map] (dc/with state arg-map))

  Searcher
  (datoms [db arg-map] (dc/datoms db arg-map))
  (pull [_db arg-map] (dc/pull state arg-map))
  (pull [_db selector eid] (dc/pull state selector eid))
  (index-range [_db _arg-map] (not-supported)))

(defprotocol Transactor
  (transact [conn arg-map])
  (release [conn])
  (db [conn])
  (with-db [conn])
  (sync [conn time-point])
  (tx-range [conn arg-map]))

(defrecord DatahikeConnection [state]
  Transactor
  (transact [_conn arg-map] (dh/transact! state arg-map))
  (release [_conn] (dh/release state))
  (db [_conn] (map->DatahikeDb {:state @state}))
  (with-db [_conn] @state)
  (sync [_conn _time-point] (not-supported))
  (tx-range [_conn _arg-map] (not-supported)))

(defrecord DatomicConnection [state]
  Transactor
  (transact [_conn arg-map] (dc/transact state arg-map))
  (release [_conn])
  (db [_conn] (map->DatomicDb {:state (dc/db state)}))
  (with-db [_conn] (dc/with-db state))
  (sync [_conn _time-point] (not-supported))
  (tx-range [_conn _arg-map] (not-supported)))

(defprotocol Connector
  (connect [client arg-map]))

(defprotocol Creator
  (administer-system [client])
  (list-databases [client arg-map])
  (create-database [client arg-map])
  (delete-database  [client arg-map]))

(defrecord DatahikeClient [state]
  Connector
  (connect [_client _arg-map]
    (map->DatahikeConnection {:state (dh/connect state)}))

  Creator
  (administer-system [_client] (not-supported))
  (list-databases [_client _arg-map] (not-supported))
  (create-database [_client arg-map]
    (dh/create-database arg-map))
  (delete-database [_client arg-map]
    (dh/delete-database arg-map)))

(defrecord DatomicClient [state]
  Connector
  (connect [_client arg-map]
    (map->DatomicConnection {:state (dc/connect state arg-map)}))

  Creator
  (administer-system [_client] (not-supported))
  (list-databases [_client _arg-map] (not-supported))
  (create-database [_client arg-map]
    (dc/create-database state arg-map))
  (delete-database [_client arg-map]
    (dc/delete-database state arg-map)))

(defn ^:export datahike-client [config]
  (map->DatahikeClient {:state config}))

(defn ^:export datomic-client [config]
  (map->DatomicClient {:state (dc/client config)}))

(defmulti -q-map (fn [{:keys [args]}] (-> args first class)))

(defmethod -q-map DatahikeDb [arg-map]
  (dh/q (update-in arg-map [:args] (fn [old] (into [(-> old first :state)] (rest old))))))

(defmethod -q-map DatomicDb [arg-map]
  (dc/q (update-in arg-map [:args] (fn [old] (into [(-> old first :state)] (rest old))))))

(defmulti -q (fn [_query & args] (-> args first class)))

(defmethod -q DatahikeDb [query & args]
  (apply dh/q query (-> args first :state) (rest args)))

(defmethod -q DatomicDb [query & args]
  (apply dc/q query (-> args first :state) (rest args)))

(defn ^:export q
  ([arg-map] (-q-map arg-map))
  ([query & args] (apply -q query args)))
