(ns wanderung.datom
  (:require [clojure.spec.alpha :as spec]))

(spec/def :datom/eid (spec/or :id number? :tempid string?))
(spec/def :datom/attribute keyword?)
(spec/def :datom/value any?)
(spec/def :datom/transaction-id :datom/eid)
(spec/def :datom/add? boolean?)

(spec/def :datom/tuple (spec/cat :eid :datom/eid
                                 :attribute :datom/attribute
                                 :value :datom/value
                                 :transaction-id :datom/transaction-id
                                 :add? :datom/add?))

(defn datom-eid [[eid _ _ _ _]] eid)
(defn datom-attribute [[_ a _ _ _]] a)
(defn datom-value [[_ _ v _ _]] v)
(defn datom-tid [[_ _ _ tid _]] tid)
(defn datom-add? [[_ _ _ _ add?]] add?)

(spec/def :datom/tuples (spec/coll-of :datom/tuple))

(defn refers-to-this-transaction? [[e _a v t _added]]
  (or (= t e)
      (= t v)))

(defn transaction-related? [[_e a _v _t _added :as datom]]
  (or (refers-to-this-transaction? datom)
      (= a :taxonomy-version/tx)))
