(ns wanderung.datom-processor
  (:require [clojure.tools.logging :as log]))

(def initial-state {:entities {}
                    :idents {}})

;;;; The idea here was that if we split a transaction,
;;;; we wanted to make sure that the retraction happened before the addition.
;;;; But that might not be the right thing to do. It might be better to
;;;; have some logic to make sure that they occur in the same transaction.
(defn presort-datom-order [[index [_e _a _v _t added]]]
  [(if added 1 0) index])

(defn presort-transaction-datoms [datoms]
  (->> datoms
       (map-indexed vector)
       (sort-by presort-datom-order)
       (map second)))

(defn ident-eid [state k]
  (get-in state [:idents k]))

(defn ident-attrib? [state x]
  (cond
    (number? x) (= x (ident-eid state :db/ident))
    (keyword? x) (= x :db/ident)
    :else (throw (ex-info "Invalid entity id" {:x x}))))

(defn inv-idents [state]
  (into {} (map (fn [[k v]] [v k])) (:idents state)))

(defn state? [x]
  (and (map? x)
       (map? (:entities x))))

(defn ref-eids [{:keys [idents entities]}]
  (let [type-ref-eid (:db.type/ref idents :db.type/ref)
        value-type-eid (:db/valueType idents :db/valueType)]
    (into #{}
          (keep (fn [[k {:keys [attributes]}]]
                  (when (= type-ref-eid (attributes value-type-eid))
                    k)))
          entities)))

(defn add-or-retract-in [dst path v added]
  (if added
    (if (= v (get-in dst path ::not-found))
      dst
      (assoc-in dst path v)) ;; <-- only assoc if the value is actually different
    (let [old-value (get-in dst path)]
      (when (not= old-value v)
        ;; This happens if we don't sort the datoms to transact so that
        ;; retractions come first
        (log/warn "Old value"
                  old-value
                  "does not match new value"
                  v  "when retracting from path" path))
      (update-in dst (butlast path) dissoc (last path)))))

(defn eid->ident [{:keys [entities idents]} eid]
  (let [ident-eid (:db/ident idents)]
    (get-in entities [eid :attributes ident-eid] eid)))

(defn ident->eid [{:keys [idents]} id]
  (if (keyword? id)
    (get idents id id)
    id))

(defn eids->ident [state eids]
  (if (coll? eids)
    (into #{} (map #(eid->ident state %)) eids)
    (eid->ident state eids)))

(defn entity-attributes
  ([state eid] (entity-attributes state eid nil))
  ([{:keys [entities idents] :as state} eid select]
   (let [eid (ident->eid state eid)
         attributes (get-in entities [eid :attributes])
         ident-eid (:db/ident idents)
         type-ref-eid (:db.type/ref idents)
         value-type-eid (:db/valueType idents)
         selected-eids (into #{} (map #(idents % %)) select)]
     (assert (= (count select) (count selected-eids)))
     (into {}
           (comp (if (seq selected-eids)
                   (filter (comp selected-eids first))
                   identity)
                 (map (fn [[k v]]
                        (let [attrib-info (get-in entities [k :attributes])
                              actual-type-ref-eid (get attrib-info value-type-eid)]
                          [(or (and (keyword? k) k)
                               (get attrib-info ident-eid)
                               k)
                           (if (= type-ref-eid actual-type-ref-eid)
                             (eids->ident state v)
                             v)]))))
           attributes))))

(defn all-entitiy-attributes [state]
  (->> state
       :entities
       keys
       (map #(entity-attributes state %))))


(def cardinalities #{:db.cardinality/many :db.cardinality/one})

(defn valid-cardinality-or-nil [x]
  (when (contains? cardinalities x)
    x))

(defn get-cardinality [state a]
  (->> [:db/cardinality]
       (entity-attributes state a)
       :db/cardinality
       valid-cardinality-or-nil))


(defn register-datom [state [e a v _tx added]]
  (case (get-cardinality state a)
    :db.cardinality/many (update-in state
                                    [:entities e :attributes a]
                                    (fn [dst]
                                      ((if added conj disj)
                                       (cond
                                         (nil? dst) #{}
                                         (number? dst) #{dst}
                                         :else dst)
                                       v)))
    
    (add-or-retract-in state [:entities e :attributes a] v added)))

(defn remove-empty-entity [state [e _ _ _ _]]
  (if (empty? (get-in state [:entities e :attributes]))
    (update state :entities dissoc e)
    state))

(defn entity-value-by-attrib-key [state eid attrib-key]
  (get-in state [:entities eid :attributes (ident-eid state attrib-key)]))

(defn detect-ident-attrib-def [state [e a v _tx added]]
  (if (and (= e a) (= v :db/ident))
    (add-or-retract-in state [:idents :db/ident] e added)
    state))

(defn detect-ident-def [state [e a v _tx added]]
  (if (ident-attrib? state a)
    (add-or-retract-in state [:idents v] e added)
    state))

(defn iterate-to-convergence [f state]
  (->> state
       (iterate f)
       (partition-all 2 1)
       (take-while (fn [[a b]] (not= a b)))
       (map second)
       (cons state)))

(defn all-ident-attribs [state]
  {:pre [(state? state)]}
  (->> state
       :idents
       vals
       (map #(entity-attributes state %))
       (into {} (map (juxt :db/ident identity)))))

(defn interpret-datom [state datom]
  (-> state
      (register-datom datom)
      (detect-ident-attrib-def datom)
      (detect-ident-def datom)
      (remove-empty-entity datom)))

(defn interpret-transaction [state data]
  (let [data (presort-transaction-datoms data)
        initial-state (reduce interpret-datom state data)
        step #(reduce detect-ident-def % data)]
    (iterate-to-convergence step initial-state)))
