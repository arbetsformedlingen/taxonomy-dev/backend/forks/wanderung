(ns wanderung.datahike
  (:require [datahike.api :as d]
            [datahike.db.utils :as dbu]))

(def find-tx-datoms
  '[:find ?tx ?inst
    :in $ ?bf
    :where
    [?tx :db/txInstant ?inst]
    [(>= ?bf ?inst)]
    [(< #inst "1970-01-02" ?inst)]])

(def find-all-tx-datoms
  '[:find ?tx ?inst
    :in $
    :where
    [?tx :db/txInstant ?inst]
    [(< #inst "1970-01-02" ?inst)]])

(def find-datoms-in-tx
  '[:find ?e ?a ?v ?t ?added
    :in $ ?t
    :where
    [?e ?a ?v ?t ?added]
    (not [?e :db/txInstant ?v ?t ?added])])

(defn get-transactions [db before-inst]
  (if before-inst
    (d/q find-tx-datoms db before-inst)
    (d/q find-all-tx-datoms db)))

(defn extract-datahike-data-from-db
  "Given a Datahike db extracts datoms from indices."
  ([db] (extract-datahike-data-from-db db nil))
  ([db before-inst]
   (let [history-db (d/history db)
         txs (sort-by
              first
              (get-transactions db before-inst))
         query {:query find-datoms-in-tx
                :args [history-db]}]
     (into []
           (comp (mapcat
                  (fn [[tid tinst]]
                    (->> (d/q (update-in query [:args] conj tid))
                         (sort-by first)
                         (into [[tid :db/txInstant tinst tid true]]))))
                 (map (fn [[e a v tx added]]
                        (let [a-ident (:ident (dbu/attr-info db a))
                              v (if (dbu/system-attrib-ref? db a-ident)
                                  (:ident (dbu/attr-info db v))
                                  v)]
                          [e a-ident v tx added]))))
           txs))))

(defn extract-datahike-data
  "Given a Datahike connection extracts datoms from indices."
  [conn] (extract-datahike-data-from-db (d/db conn)))
