(ns wanderung.datahike-test
  (:require [clojure.test :refer [deftest is testing]]
            [datahike.api :as dh]
            [datahike.db.interface :as dbi]
            [datomic.client.api :as dt]
            [taoensso.timbre :as timbre]
            [wanderung.core :as wanderung]
            [wanderung.datahike :as wd]))

(timbre/set-min-level! :warn)

(defn datomic-cfg [db-name]
  {:wanderung/type :datomic
   :server-type :dev-local
   :storage-dir :mem
   :name db-name
   :system "CI"})

(defn setup-datomic-conn
  "Given a database name creates a new datomic in-memory database and returns a connection."
  [db-name]
  (let [client-cfg (datomic-cfg db-name)
        db-cfg {:db-name db-name}
        dt-client (dt/client client-cfg)
        _ (dt/delete-database dt-client db-cfg)
        _ (dt/create-database dt-client db-cfg)]
    (dt/connect dt-client db-cfg)))

(defn datahike-cfg [db-name]
  {:wanderung/type :datahike
   :store {:backend :mem
           :id db-name}})

(defn datahike-cfgs [db-name]
  (let [base (datahike-cfg db-name)]
    [base
     (merge base
            {:keep-history true
             :attribute-refs? true})]))

(defn datahike-db-name [dh-cfg]
  (-> dh-cfg :store :id))

(defn setup-datahike-conn
  "Given a database name creates a new datahike in-memory database and returns a connection."
  [cfg]
  {:pre [(map? cfg)]}
  (dh/delete-database cfg)
  (dh/create-database cfg)
  (dh/connect cfg))

(defn setup-data [tx-fn conn]
  (tx-fn conn {:tx-data [{:db/id "datomic.tx"
                          :db/txInstant #inst "2023-01-01"}
                         {:db/ident :person/name
                          :db/valueType :db.type/string
                          :db/unique :db.unique/identity
                          :db/cardinality :db.cardinality/one}
                         {:db/ident :person/age
                          :db/valueType :db.type/long
                          :db/cardinality :db.cardinality/one}
                         {:db/ident :person/siblings
                          :db/valueType :db.type/ref
                          :db/cardinality :db.cardinality/many}]})
  (tx-fn conn {:tx-data [{:db/id "datomic.tx"
                          :db/txInstant #inst "2023-02-01"}
                         {:db/id -1
                          :person/name "Alice"
                          :person/age 25}
                         {:db/id -2
                          :person/name "Bob"
                          :person/age 35}
                         {:person/name "Charlie"
                          :person/age 45
                          :person/siblings [-1 -2]}]})
  (tx-fn conn {:tx-data [{:db/id "datomic.tx"
                          :db/txInstant #inst "2023-03-01"}
                         {:person/name "Daphne"
                          :person/age 45}]}))

(deftest test-datomic->datahike-basic
  (let [db-name "dt->dh-test-basic"
        dt-conn (setup-datomic-conn db-name)
        dh-cfg (datahike-cfg db-name)]
    (setup-datahike-conn dh-cfg)
    (setup-data dt/transact dt-conn)
    (wanderung/migrate (datomic-cfg db-name) dh-cfg)
    (testing "test basic data and query"
      (letfn [(coerce-result [result]
                (->> result
                     (map #(update (first %) :person/siblings set))
                     set))]
        (let [dh-conn (dh/connect dh-cfg)
              query '[:find (pull ?e [:person/name :person/age {:person/siblings [:person/name]}])
                      :where [?e :person/name _]]
              dt-result (->> (dt/db dt-conn)
                             (dt/q query)
                             coerce-result)
              dh-result (->> (dh/db dh-conn)
                             (dh/q query)
                             coerce-result)]
          (is (= dt-result
                 dh-result)))))))

(deftest test-datomic->datahike-history
  (let [db-name "dt->dh-test-history"
        dt-conn (setup-datomic-conn db-name)
        dh-cfg (datahike-cfg db-name)]
    (setup-datahike-conn dh-cfg)
    (setup-data dt/transact dt-conn)
    (dt/transact dt-conn {:tx-data [[:db/retractEntity [:person/name "Alice"]]]})
    (wanderung/migrate (datomic-cfg db-name) dh-cfg)
    (let [dh-conn (dh/connect dh-cfg)
          dh-db (dh/db dh-conn)
          dt-db (dt/db dt-conn)]
      (testing "current snapshot without retracted data"
        (letfn [(coerce-result [result]
                  (->> result
                       (map #(update (first %) :person/siblings set))
                       set))]
          (let [query '[:find (pull ?e [:person/name :person/age {:person/siblings [:person/name]}])
                        :where [?e :person/name _]]
                dt-result (->> dt-db
                               (dt/q query)
                               coerce-result)
                dh-result (->> dh-db
                               (dh/q query)
                               coerce-result)]
            (is (= dt-result
                   dh-result)))))
      (testing "history snapshot with retraction time"
        (let [query '[:find ?n ?d ?op
                      :where
                      [?e :person/name ?n ?t ?op]
                      [?t :db/txInstant ?d]]
              dt-result (->> dt-db
                             dt/history
                             (dt/q query)
                             set)
              dh-result (->> dh-db
                             dh/history
                             (dh/q query)
                             set)]
          (is (= dt-result
                 dh-result))))
      (testing "schema"
        (let [query '[:find (pull ?e [*])
                      :in $ [?attrs ...]
                      :where
                      [?e :db/ident ?attrs]]
              attributes [:person/name :person/age :person/siblings]
              dt-result (->> (dt/q query
                                   dt-db
                                   attributes)
                             (map first)
                             (map (fn [{:keys [db/unique] :as attr}]
                                    (cond-> attr
                                      true (dissoc :db/id)
                                      true (update :db/valueType :db/ident)
                                      true (update :db/cardinality :db/ident)
                                      (some? unique) (update :db/unique :db/ident))))
                             set)
              dh-result (->> (dh/q query
                                   dh-db
                                   attributes)
                             (map (comp #(dissoc % :db/id) first))
                             set)]
          (is (= dt-result
                 dh-result)))))))

(defn order-invariant= [a b]
  (= (frequencies a)
     (frequencies b)))

(deftest extract-datahike-data-from-db-test
  (let [db-name "my-db"]
    (doseq [dh-cfg (datahike-cfgs db-name)
            :let [dh-conn (setup-datahike-conn dh-cfg)
                  max-eid (dbi/-max-eid (dh/db dh-conn))
                  offs #(+ % max-eid)
                  _ (setup-data dh/transact dh-conn)
                  datoms (wd/extract-datahike-data-from-db (dh/db dh-conn))]]
      (is (order-invariant=
           [[536870913 :db/txInstant #inst "2023-01-01T00:00:00.000-00:00" 536870913 true]
            [(offs 1) :db/ident :person/name 536870913 true]
            [(offs 1) :db/unique :db.unique/identity 536870913 true]
            [(offs 1) :db/valueType :db.type/string 536870913 true]
            [(offs 1) :db/cardinality :db.cardinality/one 536870913 true]
            [(offs 2) :db/valueType :db.type/long 536870913 true]
            [(offs 2) :db/ident :person/age 536870913 true]
            [(offs 2) :db/cardinality :db.cardinality/one 536870913 true]
            [(offs 3) :db/valueType :db.type/ref 536870913 true]
            [(offs 3) :db/cardinality :db.cardinality/many 536870913 true]
            [(offs 3) :db/ident :person/siblings 536870913 true]
            [536870914 :db/txInstant #inst "2023-02-01T00:00:00.000-00:00" 536870914 true]
            [(offs 4) :person/name "Alice" 536870914 true]
            [(offs 4) :person/age 25 536870914 true]
            [(offs 5) :person/age 35 536870914 true]
            [(offs 5) :person/name "Bob" 536870914 true]
            [(offs 6) :person/name "Charlie" 536870914 true]
            [(offs 6) :person/siblings (offs 4) 536870914 true]
            [(offs 6) :person/age 45 536870914 true]
            [(offs 6) :person/siblings (offs 5) 536870914 true]
            [536870915 :db/txInstant #inst "2023-03-01T00:00:00.000-00:00" 536870915 true]
            [(offs 7) :person/age 45 536870915 true]
            [(offs 7) :person/name "Daphne" 536870915 true]]
           datoms)))))

(deftest extract-datahike-data-from-db-as-of-test
  (let [db-name "my-db"
        dh-cfg (datahike-cfg db-name)
        dh-conn (setup-datahike-conn dh-cfg)]
    (setup-data dh/transact dh-conn)
    (let [db (dh/db dh-conn)]
      (is (= '([536870913 :db/txInstant #inst "2023-01-01T00:00:00.000-00:00" 536870913 true]
               [1 :db/ident :person/name 536870913 true]
               [1 :db/unique :db.unique/identity 536870913 true]
               [1 :db/valueType :db.type/string 536870913 true]
               [1 :db/cardinality :db.cardinality/one 536870913 true]
               [2 :db/valueType :db.type/long 536870913 true]
               [2 :db/ident :person/age 536870913 true]
               [2 :db/cardinality :db.cardinality/one 536870913 true]
               [3 :db/valueType :db.type/ref 536870913 true]
               [3 :db/cardinality :db.cardinality/many 536870913 true]
               [3 :db/ident :person/siblings 536870913 true]
               [536870914 :db/txInstant #inst "2023-02-01T00:00:00.000-00:00" 536870914 true]
               [4 :person/name "Alice" 536870914 true]
               [4 :person/age 25 536870914 true]
               [5 :person/age 35 536870914 true]
               [5 :person/name "Bob" 536870914 true]
               [6 :person/name "Charlie" 536870914 true]
               [6 :person/siblings 4 536870914 true]
               [6 :person/age 45 536870914 true]
               [6 :person/siblings 5 536870914 true])
             (wd/extract-datahike-data-from-db db #inst "2023-02-03"))))))

(deftest test-datahike->datomic-history
  (let [db-name "dh->dt-test-history"
        dh-cfg (datahike-cfg db-name)
        dh-conn (setup-datahike-conn dh-cfg)]
    (setup-datomic-conn db-name)
    (setup-data dh/transact dh-conn)
    (dh/transact dh-conn {:tx-data [[:db/retractEntity [:person/name "Alice"]]]})
    (wanderung/migrate dh-cfg (datomic-cfg db-name))
    (let [dt-conn (dt/connect (dt/client (datomic-cfg db-name)) {:db-name db-name})
          dt-db (dt/db dt-conn)
          dh-db (dh/db dh-conn)]
      (testing "current snapshot without retracted data"
        (letfn [(coerce-result [result]
                  (->> result
                       (map #(update (first %) :person/siblings set))
                       set))]
          (let [query '[:find (pull ?e [:person/name :person/age {:person/siblings [:person/name]}])
                        :where [?e :person/name _]]
                dh-result (->> dh-db
                               (dh/q query)
                               coerce-result)
                dt-result (->> dt-db
                               (dt/q query)
                               coerce-result)]
            (is (= dh-result
                   dt-result)))))
      (testing "history snapshot with retraction time"
        (let [query '[:find ?n ?d ?op
                      :where
                      [?e :person/name ?n ?t ?op]
                      [?t :db/txInstant ?d]]
              dh-result (->> dh-db
                             dh/history
                             (dh/q query)
                             set)
              dt-result (->> dt-db
                             dt/history
                             (dt/q query)
                             set)]
          (is (= dh-result
                 dt-result))))
      (testing "schema"
        (let [query '[:find (pull ?e [*])
                      :in $ [?attrs ...]
                      :where
                      [?e :db/ident ?attrs]]
              attributes [:person/name :person/age :person/siblings]
              dh-result (->> (dh/q query
                                   dh-db
                                   attributes)
                             (map (comp #(dissoc % :db/id) first))
                             set)
              dt-result (->> (dt/q query
                                   dt-db
                                   attributes)
                             (map first)
                             (map (fn [{:keys [db/unique] :as attr}]
                                    (cond-> attr
                                      true (dissoc :db/id)
                                      true (update :db/valueType :db/ident)
                                      true (update :db/cardinality :db/ident)
                                      (some? unique) (update :db/unique :db/ident))))
                             set)]
          (is (= dh-result
                 dt-result)))))))
