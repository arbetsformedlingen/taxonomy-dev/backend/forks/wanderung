(ns wanderung.datomic-cloud-test
  (:require [clojure.test :refer [deftest is]]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [wanderung.datomic-cloud :as dc]
            [wanderung.datom-processor :as dp]))

;; These are the first 15 transactions of a real datomic database.
(def sample-txs (-> "sample_transactions.edn"
                    io/resource
                    slurp
                    edn/read-string))

;; These datoms were produced using the previous implementation
;; of `extract-datomic-cloud-data` using the first 15 transactions
;; of a real datomic database.
(def expected-datoms (->> "expected_extracted_cloud_data.edn"
                          io/resource
                          slurp
                          edn/read-string
                          (filter dc/keep-datom?)))

(def sample-datom [119 :person/name "Mjao" nil true])
(def sample-datom-2 [119 :person/mother 120 nil true])


(deftest temp-id-test
  (is (dc/temp-id? (dc/temp-id 9)))
  (is (= "tmp-9" (:value (dc/temp-id 9))))
  (is (dc/temp-id? dc/transaction-temp-id))
  (is (= "datomic.tx" (:value dc/transaction-temp-id)))
  (is (not (dc/temp-id? 9)))
  (is (not (dc/temp-id? "tmp-9")))
  (is (= [3 "tmp-119" "abc"] (dc/unwrap-temp-ids [3 (dc/temp-id 119) "abc"])))
  (is (= (dc/temp-id dc/transaction-temp-id-str)
         dc/transaction-temp-id)))

(deftest elements-are-sorted-test
  (is (not (dc/elements-are-sorted? [0 0 1 -1])))
  (is (dc/elements-are-sorted? [0 0 1]))
  (is (dc/elements-are-sorted? [:a :b]))
  (is (dc/elements-are-sorted? [:a :a]))
  (is (not (dc/elements-are-sorted? [:b :a])))
  (is (dc/elements-are-sorted? [:b]))
  (is (dc/elements-are-sorted? [])))

(defn entities-with-set-attribs [state]
  (for [x (dp/all-entitiy-attributes state)
        [_ v] x :when (set? v)]
    x))

(deftest interpreter-test
  (is (= {:a {:b 9}}
         (dp/add-or-retract-in {} [:a :b] 9 true)))
  (is (= {:a {}}
         (dp/add-or-retract-in
          {:a {:b 9}} [:a :b] 9 false)))
  (let [{:keys [_entities
                mapped-transactions
                idents
                ] :as state} (dc/interpret sample-txs)]
    (is (= 40 (:db/valueType idents)))
    (is (= 20 (:db.type/ref idents)))
    (is (= 10 (:db/ident idents)))
    (is (= (count mapped-transactions)
           (count sample-txs)))
    (let [[x0 x1 x2 x3 x4 x5 x6 x7 x8 x9] (map (comp set :data) mapped-transactions)
          mapped-transactions-p (mapv :system-transaction mapped-transactions)
          [s0 s1 s2 s3 s4 s5 s6 s7 s8 s9] mapped-transactions-p]
      (is s0)
      (is s1)
      (is s2)
      (is s3)
      (is s4)
      (is s5)
      (is (not s6))
      (is (not s7))
      (is (not s8))
      (is (not s9))
      (doseq [[i x] (map-indexed vector mapped-transactions-p)]
        (is (= x (< i 6))))
      (is (contains? x0 [40 :db/ident :db/valueType 13194139533312 true]))
      (is (contains? x1 [20 :fressian/tag :ref 13194139533313 true]))
      (is (contains? x2 [63 :db/cardinality :db.cardinality/one 13194139533314 true]))
      (is (contains? x2 [58 :fressian/tag :double 13194139533314 true]))
      (is (contains? x2 [63 :db/fulltext true 13194139533314 true]))
      (is (contains? x3 [51
                         :db/doc
                         "Property of an attribute. If true, create a fulltext search index for the attribute. Defaults to false."
                         13194139533315
                         true]))
      (is (contains? x4 [64 :db/ident :db.type/tuple 13194139533316 true]))
      (is (contains? x4 [0 :db.install/valueType :db.type/symbol 13194139533316 true]))
      (is (contains? x5 [70 :db/cardinality :db.cardinality/many 13194139533317 true]))

      ;; Schema definition
      (is (contains? x6 [82 :db/ident :concept/type 13194139533318 true]))
      (is (contains? x7 [101155069755517 :taxonomy-version/id 0 13194139533319 true]))
      (is (contains? x8 [96757023244533
                         :relation/id
                         "T2aZ_5dP_myb:broader:cYLE_n6m_XYw"
                         13194139533320
                         true]))
      (is (contains? x8 [96757023244414
                         :concept/type
                         "sun-education-field-1"
                         13194139533320
                         true]))
      (is (contains? x8 [96757023244415 :concept/id "y1xq_3kZ_e34" 13194139533320 true]))
      (is (contains? x9 [87960930223076
                         :relation/concept-2
                         87960930223074
                         13194139533321
                         true])))
    (is (= #:db{:ident :db/ident,
                :valueType :db.type/keyword,
                :cardinality :db.cardinality/one,
                :unique :db.unique/identity,
                :doc "Attribute used to uniquely name an entity."}
           (dp/entity-attributes state 10)))
    (is (= {:concept/type "driving-licence",
            :concept/definition
            "Bilar avsedda för persontransporter och som utöver förarsätet har fler än åtta sittplatser. Till en bil i denna kategori får kopplas en lätt släpvagn med en tillåten totalvikt som inte överstiger 750 kg.\r\n\r\nKörkortsålder: 21 år",
            :concept.external-standard/driving-licence-code-2013 "D",
            :concept/id "hK1a_wsQ_4UG",
            :concept/sort-order 15,
            :concept.external-standard/implicit-driving-licences
            #{87960930223120 87960930223127 87960930223072},
            :concept.external-database.ams-taxonomy-67/id "5",
            :concept/preferred-label "D",
            :concept/short-description "Buss"}
           (dp/entity-attributes state 87960930223078)))
    (is (= (dp/entity-attributes state 0 [:db/ident])
           #:db{:ident :db.part/db}))
    (is (= (-> state dc/flat-mapped-datoms first)
             [73 :db/ident :concept-type/id 13194139533318 true]))
    (is (= expected-datoms (dc/flat-mapped-datoms state)))
    (is (= #{86 69 85 119 15 113 40 13 41 122 12 19 11 16 114 42 94}
           (dp/ref-eids state)))

    (let [ents (entities-with-set-attribs state)
          cids (into #{} (keep :concept/id) ents)]
      (is (seq ents))
      (is (contains? cids "BKCx_hST_Pcm",)))))

(deftest sort-partition-by-test
  (is (= [[0 1 2] [3 4 5 6]] (dc/sort-and-partition-by #(if (< % 3) 0 1) (range 7))))
  (let [[a b :as parts] (dc/sort-and-partition-by dc/transaction-datoms-last-order expected-datoms)]
    (is (= 2 (count parts)))
    (is (= 18 (count b)))
    (is (= #{:daynote/comment :db/txInstant :taxonomy-version/tx}
           (into #{}  (map second) b)))
    (is (= (count expected-datoms)
           (+ (count a) (count b))))))

(defn check-transactions-result [result]
  (is (= (dp/entity-attributes result 96)
         #:db{:ident
              :concept.external-standard/national-nuts-level-3-code-2019,
              :valueType :db.type/string,
              :cardinality :db.cardinality/one,
              :doc "Swedish Län code"}))
  (is (= (dp/entity-attributes result :taxonomy-version/id)
         #:db{:ident :taxonomy-version/id,
              :valueType :db.type/long,
              :cardinality :db.cardinality/one,
              :unique :db.unique/identity,
              :doc
              "The version identifier of the taxonomy, used almost like a tag in Git."}))
  (is (= (dp/get-cardinality result :daynote/comment)
         :db.cardinality/one))
  (let [ents (entities-with-set-attribs result)
        m (into {} (map (juxt :concept/id identity)) ents)]
    (is (seq ents))
    (is (= {:concept/type "driving-licence",
            :concept/definition
            "Bilar med en tillåten totalvikt över 3 500 kg med undantag av sådana som avses i kategori D. Till en bil i denna kategori får kopplas en lätt släpvagn med en tillåten totalvikt av högst 750 kg.\r\n\r\nKörkortsålder: 18 år",
            :concept.external-standard/driving-licence-code-2013 "C",
            :concept/id "BKCx_hST_Pcm",
            :concept/sort-order 11,
            :concept.external-standard/implicit-driving-licences
            #{87960930223120 87960930223072 87960930223105},
            :concept.external-database.ams-taxonomy-67/id "4",
            :concept/preferred-label "C",
            :concept/short-description "Tung lastbil"}
           (m "BKCx_hST_Pcm"))))
  (is (every? string? (keys (:temp-ids result))))
  (is (every? number? (vals (:temp-ids result)))))

(deftest run-transaction-test
  (let [system-attribs (dc/load-system-attribs)]
    (is (map? system-attribs)))
  (let [result (dc/push (dc/initialize-push-state {:target dc/data-transaction-target})
                        expected-datoms)]
    (check-transactions-result result)
    (is (= 9 (-> result :target :transactions count)))
    (let [referred-txs (into #{}
                             (keep (fn [[_ a v _ _]] (when (= :taxonomy-version/tx a) v)))
                             expected-datoms)
          not-expected-txids (into #{}
                                   (comp (map #(-> % (nth 3) dc/temp-id :value)))
                                   expected-datoms)
          expected-tmp-ids (-> #{"datomic.tx"}
                               (into  (comp (map (comp :value dc/temp-id))
                                            (remove not-expected-txids))
                                      (-> result :entities keys))
                               (into (map (comp :value dc/temp-id)) referred-txs))
          actual-tmp-ids (-> result :temp-ids keys set)]
      (is (= expected-tmp-ids actual-tmp-ids))))
  (let [result (dc/push (dc/initialize-push-state
                         {:target dc/data-transaction-target
                          :max-transaction-size 10000})
                        expected-datoms)]
    (check-transactions-result result)
    (is (= 10 (-> result :target :transactions count))))
  (let [result (dc/push (dc/initialize-push-state
                         {:target dc/data-transaction-target
                          :max-transaction-size 100})
                        expected-datoms)
        result2 (dc/push (dc/initialize-push-state
                         {:target dc/data-transaction-target
                          :max-transaction-size 10})
                        expected-datoms)]
    (check-transactions-result result)
    (check-transactions-result result2)
    (is (< 9 (-> result :target :transactions count)))
    (is (< (-> result :target :transactions count)
           (-> result2 :target :transactions count)))))


(deftest misc-test
  (is (dc/transaction? {:t 989 :data []}))
  (is (not (dc/transaction? {:t 989})))
  (is (not (dc/transaction? []))))

(deftest test-split-into-batches
  (is (= [[]] (dc/with-default-batch (into [] (dc/split-into-batches 10 2) []))))
  (is (= [[]] (dc/with-default-batch (into [] (dc/split-into-batches nil 2) []))))
  (is (= [[:a :b :c]] (dc/with-default-batch (into [] (dc/split-into-batches nil 2)  [[:a] [:b :c]]))))
  (is (= [[:a] [:b]] (dc/with-default-batch (into  [] (dc/split-into-batches 3 2)  [[:a] [:b]])))))

(deftest test-sort-and-wrap-atomic-ops
  (do (is (= [] (dc/sort-and-wrap-atomic-ops {} [])))
      (is (= '(([1 :parent 3 119 false] [1 :parent 2 119 true]))
             (dc/sort-and-wrap-atomic-ops
              dp/initial-state
              [[1 :parent 2 119 true] [1 :parent 3 119 false]])))
      (is (= '([[1 :child 3 119 false]] [[1 :parent 2 119 true]])
             (dc/sort-and-wrap-atomic-ops
              dp/initial-state
              [[1 :parent 2 119 true] [1 :child 3 119 false]])))))

(deftest test-weighted-partitioning
  (is (= [] (into [] (dc/weighted-partitioning identity 10) [])))
  (is (= [[5]] (into [] (dc/weighted-partitioning identity 10) [5])))
  (is (= [[5] [6]] (into [] (dc/weighted-partitioning identity 10) [5 6])))
  (is (= [[5 3]] (into [] (dc/weighted-partitioning identity 10) [5 3])))
  (is (= [[5 3] [8] [9 1]]
         (into [] (dc/weighted-partitioning identity 10) [5 3 8 9 1]))))
