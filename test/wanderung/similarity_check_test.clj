(ns wanderung.similarity-check-test
  (:import [java.util ArrayList])
  (:require [wanderung.similarity-check :as sc]
            [wanderung.datom :as datom]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [wanderung.datom-processor :as dp]
            [clojure.test :refer [deftest is]]))

(def sample-graph-a {:vertices {1 "abc"
                                2 "xyz"
                                3 "abc"}
                     :directed-edges [[1 :mjao 2]]})
(def sample-graph-b {:vertices {20 "xyz"
                                119 "abc"
                                120 "abc"}
                     :directed-edges [[120 :mjao 20]]})

(deftest misc-test
  (is (sc/valid-directed-graph? sample-graph-a))
  (is (= (sc/invert-map {:a 1 :b 1})
         {1 [:a :b]}))
  (is (sc/directed-graph? sample-graph-a))
  (let [m (sc/integer-vertex-map sample-graph-b)]
    (is (= #{20 119 120} (-> m keys set)))
    (is (= #{0 1 2} (-> m vals set)))
    ))

(deftest solve-test
  (is (= [{1 120, 2 20, 3 119}]
         (sc/match-graphs sample-graph-a sample-graph-b)))
  (is (empty?
       (sc/match-graphs
        sample-graph-a
        (assoc sample-graph-b
               :directed-edges
               [[120 :katt 20]])))))

(def sample-datoms (->> "expected_extracted_cloud_data.edn"
                        io/resource
                        slurp
                        edn/read-string))

(defn eid-mapper [offset]
  (fn [x]
    (if (number? x)
      (+ x offset)
      x)))

(def expected-ref-eids #{86 85 119 113 122 114 94})

(deftest real-matching-test
  (let [offset 2342375
        forward (eid-mapper offset)
        backward (eid-mapper (- offset))
        state (sc/accumulate-datoms sample-datoms)
        state2 (sc/map-eids state forward)
        entity-to-compare 79164837204156

        get-attribs (fn [state eid]
                      (get-in state
                              [:entities eid :attributes]))
        entity (get-attribs state entity-to-compare)
        entity2 (get-attribs state2 (forward entity-to-compare))

        edges (-> state sc/labeled-edges set)
        edges2 (-> state2 sc/labeled-edges set)]

    ;; Check that edges are properly listed
    (is (contains? edges [79164837202812
                          :relation/concept-1
                          79164837202811]))
    (is (< 0 (count edges)))
    (is (= (count edges) (count edges2)))
    (doseq [edge-set [edges edges2]
            [src label dst] edge-set]
      (is (number? src))
      (is (keyword? label))
      (is (number? dst)))
    (doseq [[src label dst] edges]
      (is (edges2 [(forward src) label (forward dst)])))

    ;; Make sure that mapping eids back and forth works
    (is (->> state :entities count (= 5259)))
    (is (not= state state2))
    (is (= state (sc/map-eids state2 backward)))

    ;; Verify that mapping works for an individual entity
    (is (= #:relation{:id "uhiW_P9y_2Yf:broader:7wHq_Mri_wCt",
                      :concept-1 79164837204155,
                      :concept-2 79164837201331,
                      :type "broader"}
           entity))
    (is (-> entity
            (update :relation/concept-1 forward)
            (update :relation/concept-2 forward)
            (= entity2)))

    ;; Make sure that the ref-eids are what they should
    ;; and that they get mapped.
    (is (= expected-ref-eids
           (dp/ref-eids state)))
    (is (= expected-ref-eids
           (into #{}
                 (map backward)
                 (dp/ref-eids state2))))
    
    ;; Check that features are invariant under eid mapping.
    (let [fm (sc/entity-feature-map state)
          features (-> fm vals set)
          features2 (-> state2 sc/entity-feature-map vals set)
          freqs (-> fm vals frequencies)]
      (is (= features features2))
      (is (= (count features) (count freqs)))
      (is (-> freqs vals set (= #{1}))))

    (let [a (sc/datom-match-data-from-state state)
          b (sc/datom-match-data-from-state state2)
          solution (sc/solve-graph-matching-problem a b)
          vertex-maps (:vertex-maps solution)
          m (first vertex-maps)]
      (is (= m (into {}
                     (map (juxt identity forward))
                     (keys (:entities state)))))
      (is (= 1 (count vertex-maps)))
      (is (empty? (seq (:missing-features solution)))))))
