(ns wanderung.datom-test
  (:require [clojure.test :refer [deftest is]]
            [clojure.spec.alpha :as spec]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [wanderung.datom :as datom]))

(def testdata (-> "testdatoms.edn"
                  io/resource
                  slurp
                  edn/read-string))

(deftest valid-tuples-test
  (is (spec/valid? :datom/tuples testdata)))


