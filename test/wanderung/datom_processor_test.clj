(ns wanderung.datom-processor-test
  (:require [wanderung.datom-processor :as dp]
            [clojure.test :refer [deftest is]]))

(defn step [x]
  (min (inc x) 6))

(deftest iterate-to-convergence-test
  (is (= (dp/iterate-to-convergence step 0)
         [0 1 2 3 4 5 6])))

